<!ENTITY torsettings.dialog.title "Axustes da rede Tor">
<!ENTITY torsettings.wizard.title.default "Conectar con Tor">
<!ENTITY torsettings.wizard.title.configure "Axustes da rede Tor">
<!ENTITY torsettings.wizard.title.connecting "Estabelecendo unha conexión">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Idioma do navegador Tor">
<!ENTITY torlauncher.localePicker.prompt "Seleccione un idioma.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Prema sobre «Conectar» para conectar con Tor.">
<!ENTITY torSettings.configurePrompt "Prema sobre «Configurar» para axustar a configuración da rede se está nun país que censure Tor (como Exipto, China, Turquía) ou se está a conectar desde unha rede privada que requira un servidor intermedio. ">
<!ENTITY torSettings.configure "Configurar">
<!ENTITY torSettings.connect "Conectar">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Á espera de que Tor comece ...">
<!ENTITY torsettings.restartTor "Reiniciar Tor">
<!ENTITY torsettings.reconfigTor "Reconfigurar">

<!ENTITY torsettings.discardSettings.prompt "Ten configuradas pontes Tor ou introduciu os axustes dun servidor intermedio local. &#160; Para facer unha conexión directa coa rede Tor, esta configuración debe ser retirada.">
<!ENTITY torsettings.discardSettings.proceed "Retirar a configuración e conectar">

<!ENTITY torsettings.optional "Opcional">

<!ENTITY torsettings.useProxy.checkbox "Emprego un proxy para conectarme a Internet.">
<!ENTITY torsettings.useProxy.type "Tipo de Proxy">
<!ENTITY torsettings.useProxy.type.placeholder "seleccione un tipo de servidor intermedio">
<!ENTITY torsettings.useProxy.address "Enderezo">
<!ENTITY torsettings.useProxy.address.placeholder "Enderezo IP ou nome da máquina">
<!ENTITY torsettings.useProxy.port "Porto">
<!ENTITY torsettings.useProxy.username "Nome de usuario">
<!ENTITY torsettings.useProxy.password "Contrasinal">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Este computador pasa por un firewall que só permite conexións a certos portos">
<!ENTITY torsettings.firewall.allowedPorts "Portos permitidos">
<!ENTITY torsettings.useBridges.checkbox "Tor está censurado no meu país">
<!ENTITY torsettings.useBridges.default "Seleccione unha ponte incorporada">
<!ENTITY torsettings.useBridges.default.placeholder "Seleccione unha ponte">
<!ENTITY torsettings.useBridges.bridgeDB "Solicite unha ponte de torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Escriba os caracteres da imaxe">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Obter un novo reto">
<!ENTITY torsettings.useBridges.captchaSubmit "Enviar">
<!ENTITY torsettings.useBridges.custom "Fornecer unha ponte coñecida">
<!ENTITY torsettings.useBridges.label "Engadir información dunha ponte dende unha fonte fiábel">
<!ENTITY torsettings.useBridges.placeholder "escribir enderezo:porto (un por liña)">

<!ENTITY torsettings.copyLog "Copia o rexistro do Tor ao portapapeis">

<!ENTITY torsettings.proxyHelpTitle "Axuda do proxy">
<!ENTITY torsettings.proxyHelp1 "Un proxy local pode ser necesario cando se conecte a través da rede dunha empresa, escola ou universidade.&#160;Se non está seguro cando un servidor intermedio é necesario, mire a configuración da Internet noutro navegador ou comprobe a configuración de rede do seu sistema.">

<!ENTITY torsettings.bridgeHelpTitle "Axuda do Repetidor Ponte">
<!ENTITY torsettings.bridgeHelp1 "As pontes son reenviadores que fan máis difícil bloquear as conexións á rede Tor.&#160;Cada tipo de ponte utiliza un método diferente para evitar a censura.&#160;As obfs por exemplo fan que o seu tráfico pareza un ruído aleatorio, e os meek fan o que o seu tráfico pareza que se estea conectando a ese servizo en lugar de a Tor.">
<!ENTITY torsettings.bridgeHelp2 "Debido a que certos países tentar bloquear Tor, algunhas pontes funcionan nalgúns países mais non noutros.&#160;Se non está seguro de que pontes funcionan no seu país, visite torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Agarde mentres se estabelece unha conexión coa rede Tor.&#160; Pode levar algúns minutos.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Conexión">
<!ENTITY torPreferences.torSettings "Configuración do Tor">
<!ENTITY torPreferences.torSettingsDescription "O navegador Tor enruta o seu tráfico sobre a rede Tor, xestionada por milleiros de voluntarios de todo o mundo." >
<!ENTITY torPreferences.learnMore "Aprender máis">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Proba">
<!ENTITY torPreferences.statusInternetOnline "Conectado">
<!ENTITY torPreferences.statusInternetOffline "Desconectado">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Conectado">
<!ENTITY torPreferences.statusTorNotConnected "Not Connected">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Aprender máis">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Pontes">
<!ENTITY torPreferences.bridgesDescription "As pontes axudan a acceder á rede Tor en lugares onde Tor está bloqueado. Dependendo de onde estea vostede, unha ponte pode funcionar mellor ca outra.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automática">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Quitar">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Copiado!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Solicitar unha ponte...">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avanzados">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Ver rexistros...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Cancelar">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Pedir unha ponte">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contactando con BridgeDB. Agarde.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Resolva o CAPTCHA para pedir unha ponte.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "A solución non é correcta. Ténteo de novo outra vez.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Escriba información dunha orixe fiábel">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Rexistros do Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Not Connected">
<!ENTITY torConnect.connectingConcise "Conectando...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automática">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Inténtao de novo">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
