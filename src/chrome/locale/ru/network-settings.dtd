<!ENTITY torsettings.dialog.title "Настройки сети Tor">
<!ENTITY torsettings.wizard.title.default "Подключение к Tor">
<!ENTITY torsettings.wizard.title.configure "Настройки сети Tor">
<!ENTITY torsettings.wizard.title.connecting "Идет подключение">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Язык Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "Пожалуйста, выберите язык.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Нажмите &quot;Соединиться&quot; для подключения к Tor.">
<!ENTITY torSettings.configurePrompt "Нажмите &quot;Настроить&quot; для настройки сети. Это имеет смысл, если вы в стране, запрещающей Tor (например, Египет, Китай, Турция), или если вы подключаетесь из приватной сети, требующей прокси.">
<!ENTITY torSettings.configure "Настроить">
<!ENTITY torSettings.connect "Соединиться">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Ожидание запуска Tor...">
<!ENTITY torsettings.restartTor "Перезапустить Tor">
<!ENTITY torsettings.reconfigTor "Изменить настройки">

<!ENTITY torsettings.discardSettings.prompt "Вы настроили мосты Tor или локальный прокси-сервер. Для прямого подключения к сети Tor эти параметры нужно удалить.">
<!ENTITY torsettings.discardSettings.proceed "Удалить настройки и подключиться">

<!ENTITY torsettings.optional "Дополнительно">

<!ENTITY torsettings.useProxy.checkbox "Я использую прокси для подключения к интернету">
<!ENTITY torsettings.useProxy.type "Тип прокси">
<!ENTITY torsettings.useProxy.type.placeholder "выбор типа прокси">
<!ENTITY torsettings.useProxy.address "Адрес">
<!ENTITY torsettings.useProxy.address.placeholder "IP-адрес или хост">
<!ENTITY torsettings.useProxy.port "Порт">
<!ENTITY torsettings.useProxy.username "Имя пользователя">
<!ENTITY torsettings.useProxy.password "Пароль">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Мой брандмауэр разрешает подключения только к определенным портам">
<!ENTITY torsettings.firewall.allowedPorts "Разрешенные порты">
<!ENTITY torsettings.useBridges.checkbox "Tor заблокирован в моей стране">
<!ENTITY torsettings.useBridges.default "Выбрать встроенный мост">
<!ENTITY torsettings.useBridges.default.placeholder "выбор моста">
<!ENTITY torsettings.useBridges.bridgeDB "Запросить мост у torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Введите символы с изображения">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Показать другую картинку">
<!ENTITY torsettings.useBridges.captchaSubmit "ОК">
<!ENTITY torsettings.useBridges.custom "Указать мост вручную">
<!ENTITY torsettings.useBridges.label "Укажите данные моста из доверенного источника">
<!ENTITY torsettings.useBridges.placeholder "адрес:порт (по одному в строке)">

<!ENTITY torsettings.copyLog "Скопировать журнал Tor в буфер обмена">

<!ENTITY torsettings.proxyHelpTitle "Помощь по прокси">
<!ENTITY torsettings.proxyHelp1 "Локальный прокси-сервер может понадобиться при подключении через корпоративную, школьную или университетскую сеть.&#160;Если вы не уверены, нужен ли прокси, посмотрите сетевые настройки в другом браузере или проверьте сетевые настройки вашей системы.">

<!ENTITY torsettings.bridgeHelpTitle "Помощь по мостам (ретрансляторам)">
<!ENTITY torsettings.bridgeHelp1 "Мосты – непубличные точки-посредники (ретрансляторы), которые затрудняют попытки цензоров блокировать подключения к сети Tor. Каждый тип моста использует отличный от других метод для обхода блокировки. Мосты типа &quot;obfs&quot; делают ваш трафик похожим на случайный шум. Мосты типа &quot;meek&quot; имитируют подключение к сервису, отличному от Tor.">
<!ENTITY torsettings.bridgeHelp2 "Разные страны по-разному пытаются блокировать Tor. Поэтому те или иные мосты работают в одних странах, но не работают в других. Если не уверены в том, какой мост сработает у вас в стране, посетите torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Пожалуйста, подождите, пока мы установим подключение к сети Tor. Это может занять несколько минут.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Подключение">
<!ENTITY torPreferences.torSettings "Настройки Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser перенаправляет ваш трафик через сеть Tor. Ее поддерживают тысячи добровольцев по всему миру." >
<!ENTITY torPreferences.learnMore "Узнать больше">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Интернет:">
<!ENTITY torPreferences.statusInternetTest "Тест">
<!ENTITY torPreferences.statusInternetOnline "В сети">
<!ENTITY torPreferences.statusInternetOffline "Не в сети">
<!ENTITY torPreferences.statusTorLabel "Сеть Tor:">
<!ENTITY torPreferences.statusTorConnected "Подключено">
<!ENTITY torPreferences.statusTorNotConnected "Не подключен">
<!ENTITY torPreferences.statusTorBlocked "Потенциально заблокирован">
<!ENTITY torPreferences.learnMore "Узнать больше">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Быстрый старт">
<!ENTITY torPreferences.quickstartDescriptionLong "Быстрый запуск автоматически подключит Tor Browser к сети Tor при запуске, исходя из последних использованных настроек подключения.">
<!ENTITY torPreferences.quickstartCheckbox "Всегда подключаться автоматически">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Мосты">
<!ENTITY torPreferences.bridgesDescription "Мосты помогают получить доступ к сети Tor там, где он заблокирован. В зависимости от вашего местонахождения один мост может работать лучше другого.">
<!ENTITY torPreferences.bridgeLocation "Ваше местоположение">
<!ENTITY torPreferences.bridgeLocationAutomatic "Автоматически">
<!ENTITY torPreferences.bridgeLocationFrequent "Часто выбираемое местоположение">
<!ENTITY torPreferences.bridgeLocationOther "Другие места">
<!ENTITY torPreferences.bridgeChooseForMe "Выбрать мост для меня...">
<!ENTITY torPreferences.bridgeBadgeCurrent "Ваши текущие мосты">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "Вы можете сохранить один или несколько мостов, и Tor сам выберет, какой из них использовать при подключении. Tor автоматически переключится на использование другого моста, когда это необходимо.">
<!ENTITY torPreferences.bridgeId "#1 мост: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Удалить">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Отключить встроенные мосты">
<!ENTITY torPreferences.bridgeShare "Поделитесь этим мостом, используя QR-код или скопировав его адрес:">
<!ENTITY torPreferences.bridgeCopy "Скопировать адрес моста">
<!ENTITY torPreferences.copied "Скопировано!">
<!ENTITY torPreferences.bridgeShowAll "Показать все мосты">
<!ENTITY torPreferences.bridgeRemoveAll "Удалить все мосты">
<!ENTITY torPreferences.bridgeAdd "Добавить новый мост">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Выберите один из встроенных мостов Tor Browser">
<!ENTITY torPreferences.bridgeSelectBuiltin "Выбрать встроенный мост…">
<!ENTITY torPreferences.bridgeRequest "Запрос моста…">
<!ENTITY torPreferences.bridgeEnterKnown "Введите адрес моста, который вам уже известен">
<!ENTITY torPreferences.bridgeAddManually "Добавить мост вручную…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Дополнительно">
<!ENTITY torPreferences.advancedDescription "Настройка подключения Tor Browser к интернету">
<!ENTITY torPreferences.advancedButton "Настройки…">
<!ENTITY torPreferences.viewTorLogs "Просмотр журналов Tor">
<!ENTITY torPreferences.viewLogs "Смотреть журнал…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Удалить все мосты?">
<!ENTITY torPreferences.removeBridgesWarning "Это действие не может быть отменено.">
<!ENTITY torPreferences.cancel "Отмена">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Сканировать QR-код">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Встроенные мосты">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser включает в себя несколько специальных типов мостов, известных как &quot;подключаемые транспорты&quot;.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 — это тип встроенного моста, благодаря которому ваш трафик Tor выглядит случайным. Кроме того, вероятность его блокировки ниже, чем у его предшественника, моста obfs3.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake — это встроенный мост, который обходит цензуру, направляя ваше соединение через прокси-серверы Snowflake, которыми управляют добровольцы">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure — это встроенный мост, который делает ваш трафик неотличимым от трафика в направлении Microsoft.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Запрос моста">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Обращение к BridgeDB. Пожалуйста, подождите.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Решите CAPTCHA для запроса моста">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Неправильно. Пожалуйста, попробуйте снова.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Предоставить мост">
<!ENTITY torPreferences.provideBridgeHeader "Укажите данные моста из доверенного источника">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Настройки подключения">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Настройте способ подключения Tor Browser к интернету">
<!ENTITY torPreferences.firewallPortsPlaceholder "Значения, разделенные запятыми">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Журнал Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Не подключен">
<!ENTITY torConnect.connectingConcise "Соединение...">
<!ENTITY torConnect.tryingAgain "Повторная попытка...">
<!ENTITY torConnect.noInternet "Tor Browser не может подключиться к Интернету">
<!ENTITY torConnect.couldNotConnect "Tor Browser не может подключиться к Tor">
<!ENTITY torConnect.assistDescriptionConfigure "настроить подключение"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Если Tor заблокирован в месте вашего нахождения, попробуйте использовать мост. Помощник подключения поможет подобрать мост, используя ваше местоположение, или вы можете выбрать #1 вручную."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Пробуем мост...">
<!ENTITY torConnect.tryingBridgeAgain "Пробуем еще раз…">
<!ENTITY torConnect.errorLocation "Tor Browser не смог определить ваше местоположение">
<!ENTITY torConnect.errorLocationDescription "Браузеру Tor необходимо знать ваше местоположение, чтобы подобрать для вас подходящий мост. Если вы предпочитаете не сообщать ваше местоположение, то выберите #1 вручную."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Эти настройки местоположения верны?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser все еще не может подключиться к Tor. Пожалуйста, проверьте правильность настроек местоположения и повторите попытку или #1 вместо этого."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Помощник подключения">
<!ENTITY torConnect.breadcrumbLocation "Настройки местоположения">
<!ENTITY torConnect.breadcrumbTryBridge "Попробовать мост">
<!ENTITY torConnect.automatic "Автоматически">
<!ENTITY torConnect.selectCountryRegion "Выбор страны или региона">
<!ENTITY torConnect.frequentLocations "Часто выбираемые местоположения">
<!ENTITY torConnect.otherLocations "Другие места">
<!ENTITY torConnect.restartTorBrowser "Перезапустить Tor Browser">
<!ENTITY torConnect.configureConnection "Настройка подключения…">
<!ENTITY torConnect.viewLog "Просмотр журналов…">
<!ENTITY torConnect.tryAgain "Попробовать снова">
<!ENTITY torConnect.offline "Интернет недоступен">
<!ENTITY torConnect.connectMessage "Изменения в настройках Tor не вступят в силу, пока вы не подключитесь">
<!ENTITY torConnect.tryAgainMessage "Tor Browser не удалось установить соединение с сетью Tor">
<!ENTITY torConnect.yourLocation "Ваше местоположение">
<!ENTITY torConnect.tryBridge "Попробовать мост">
<!ENTITY torConnect.autoBootstrappingFailed "Автоматическая настройка не удалась">
<!ENTITY torConnect.autoBootstrappingFailed "Автоматическая настройка не удалась">
<!ENTITY torConnect.cannotDetermineCountry "Невозможно определить страну пользователя">
<!ENTITY torConnect.noSettingsForCountry "Нет доступных настроек для вашего местоположения">
